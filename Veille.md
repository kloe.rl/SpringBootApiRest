# Veille

## Qu'est-ce un ORM ?

L'**ORM**, ou **Object-Relational Mapping**, est une technique utilisée en programmation pour faire le lien entre les objets du code et les données de la Bases de données relationnelles.

**L’ORM** permet de manipuler les données en base comme si c’était des objets sans avoir à écrire des requêtes SQL complexes. **L’ORM** permet de créer des modèles qui correspondent aux tables de la BDD.

### Avantages

- **Abstraction** : Pas de gestion de script SQL.
- **Sécurité** : l’utilisation d’un ORM permet de prévenir les injections SQL, puisque les requêtes sont générées automatiquement et les données sont souvent échappées automatiquement.
- **Productivité** : comme on ne génère pas les requêtes SQL, on peut se concentre davantage sur la logique métier.

### Inconvénients

- **Performance** : parfois les requêts générées automatiquement ne sont pas optimisées et un SQL manuel est parfois requis.
- **Complexité cachée** : l’abstraction peut parfois cachée des détails importants sur la manière dont les données dont manipulées, ce qui peut conduire à des erreurs non anticipées lors de cas d'usage plus complexes.

## Qu'est-ce que Hibernate ?

**Hibernate** est un framework **ORM** léger, open-source et avec des fonctionnalités avancées pour Java qui permet de mettre en place la persistance des données. C’est une **implémentation JPA**, il va donc au de-là en offrant des optimisations et des fonctionnalités additionnelles. Il permet de gérer des interactions complexes avec la BDD.

Il utilise un langage de requête semblable à SQL, le HQL (Hibernate Query Language). Hibernate est totalement compatible avec les notions orientées objet telles que l’héritage, le polymorphisme etc.

## Qu'est-ce que Spring Data JPA, quel est son rapport avec Hibernate ?

C’est une specification qui definit une interface standard pour que les applications JAVA puissent interagir avec les bases de données. Elle simplifie l’accès à la base de données en fournissant des annotations et des API pour mapper des objets Java en table. 

**JPA** n’est pas une implementation mais plutôt un ensemble de règles qu’un framework comme Hibernate implémente.

**JPA vs. Hibernate :**

**JPA** : Spécification définissant un ensemble de règles permettant aux applications Java d'interagir avec les bases de données.
**Hibernate** : Un cadre ORM à part entière qui met en œuvre la spécification JPA avec des fonctionnalités supplémentaires.

## Qu'est-ce que JDBC, quel est son rapport avec Hibernate ?

**JDBC** (**Java Database Connectivity**) est l’API standard pour établir des connections avec des BDD relationnelles. Contrairement à JPA et à Hibernate, **JDBC** est un framework bas-niveau qui exige des développeurs l’écriture des requêtes SQL et la gestion des ensembles de résultats manuellement.

S'il offre plus de contrôle, il exige aussi plus de code. **JDBC** est souvent utilisé lorsqu'un contrôle fin des interactions avec la base de données est nécessaire, ou lorsque l'on travaille avec des bases de données qui ne sont pas couvertes par des frameworks de plus haut niveau.

**JPA vs. JDBC :**

**JPA** : abstraction de plus haut niveau avec mappage objet-relationnel, réduisant le besoin de SQL manuel.
**JDBC** : API de niveau inférieur nécessitant des requêtes SQL manuelles et la gestion des ensembles de résultats.