package co.simplon.SpringBootApiRest.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import co.simplon.SpringBootApiRest.model.Author;

public interface AuthorRepository extends JpaRepository<Author, Long> {

}
