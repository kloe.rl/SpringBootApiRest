package co.simplon.SpringBootApiRest.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import co.simplon.SpringBootApiRest.model.Category;

public interface CategoryRepository extends JpaRepository<Category, Long> {
    
    public List<Category> findByNameContainingIgnoreCase(String name);

}