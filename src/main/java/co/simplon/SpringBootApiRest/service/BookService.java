package co.simplon.SpringBootApiRest.service;

import java.util.List;
import java.util.Objects;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import co.simplon.SpringBootApiRest.model.Book;
import co.simplon.SpringBootApiRest.repository.BookRepository;


@Service
public class BookService {
    @Autowired
    private BookRepository bookRepository;

    // Get all books
    public List<Book> getBook() {
        return (List<Book>) bookRepository.findAll();
    }
    
    // Get One book
    public Optional<Book> getBookById(Long id) {
        return bookRepository.findById(id);
    }

    // Add a new book
    public Book saveBook(Book book) {
        Book newBook = bookRepository.save(book);
        return newBook;
    }

    // Modify a book 
    public Book updateBook(Book book, Long id) {
        Book bookDB = bookRepository.findById(id).get();
        if (Objects.nonNull(book.getTitle())
        && !"".equalsIgnoreCase(
            book.getTitle())) {
        bookDB.setTitle(
            book.getTitle());
    }

    if (Objects.nonNull(
            book.getDescription())
        && !"".equalsIgnoreCase(
            book.getDescription())) {
        bookDB.setDescription(
            book.getDescription());
    }

    if (Objects.nonNull(book.getAvailable())) {
            bookDB.setAvailable(book.getAvailable());
    }
        return bookRepository.save(bookDB);
    }

    // Delete a book
    public void deleteBook(Long id) {
        bookRepository.deleteById(id);
    }

    // Get book by Title
    public List<Book> findByTitle(String title) {
        return bookRepository.findByTitleIs(title);
    }
}
