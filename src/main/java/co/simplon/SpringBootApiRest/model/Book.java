package co.simplon.SpringBootApiRest.model;

import java.util.Set;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.JoinTable;
import jakarta.persistence.ManyToMany;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.Table;

@Entity
@Table(name = "books")
public class Book {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(length=100)
    private String title;

    @Column(columnDefinition = "TEXT")
    private String description;

    @Column(columnDefinition = "BOOLEAN DEFAULT true")
    private Boolean available = true;

    @ManyToOne
    // @JoinColumn(name = "category")
    @JsonIgnoreProperties({ "books" })
    private Category category;

    @ManyToMany
    @JoinTable(
        name = "book_has_author",
        joinColumns = @JoinColumn(name = "book_id"),
        inverseJoinColumns = @JoinColumn(name = "author_id")
    )

    @JsonIgnoreProperties({ "books" })
    private Set<Author> authors;

    public Book() {
    }

    public Long getId() {
        return this.id;
    }

    public Long setId(Long id) {
        return this.id = id;
    }

    public String getTitle() {
        return this.title;
    }

    public String setTitle(String title) {
        return this.title = title;
    }

    public String getDescription() {
        return this.description;
    }

    public String setDescription(String description) {
        return this.description = description;
    }

    public Boolean isAvailable(Boolean available) {
       return this.available = available;
    }

    public boolean getAvailable() {
        return this.available;
    }

    public boolean setAvailable(boolean available) {
        return this.available = available;
    }

    public Category getCategory() {
        return this.category;
    }

    public Category setCategory(Category category) {
        return this.category = category;
    }

    public Set<Author> getAuthors() {
        return this.authors;
    }

    public void setAuthors(Set<Author> authors) {
        this.authors = authors;
    }

}
