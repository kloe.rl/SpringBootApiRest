package co.simplon.SpringBootApiRest.controller;

import org.springframework.web.bind.annotation.RestController;

import co.simplon.SpringBootApiRest.model.Category;
import co.simplon.SpringBootApiRest.repository.CategoryRepository;

import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;



@RestController
@RequestMapping("/categories")
public class CategoryController {


    private CategoryRepository categoryRepository;

    public CategoryController(CategoryRepository categoryRepositoryInjected) {
        this.categoryRepository = categoryRepositoryInjected;
    }

    @GetMapping("")
    public List<Category> getAll() {
        return this.categoryRepository.findAll();
    }

    @PostMapping("")
    public Category postOne(@RequestBody Category category) {
        return this.categoryRepository.save(category);
    }
    
}
