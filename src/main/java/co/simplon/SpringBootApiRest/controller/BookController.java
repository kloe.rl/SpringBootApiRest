package co.simplon.SpringBootApiRest.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import co.simplon.SpringBootApiRest.model.Book;
import co.simplon.SpringBootApiRest.service.BookService;

@RestController
public class BookController {
    @Autowired
    private BookService bookService;

    // GET All
    @GetMapping("/books")
    public List<Book> getBook() {
        return bookService.getBook();
    }

    // GET One
    @GetMapping("/books/{id}")
    public Optional<Book> getBookById(@PathVariable("id") Long id) {
        return bookService.getBookById(id);
    }

    // POST
    @PostMapping("/books")
    public Book saveBook(@RequestBody Book book) {
        return bookService.saveBook(book);
    }

    // UPDATE
    @PutMapping("/books/{id}") 
    public Book updateBook(@RequestBody Book book, @PathVariable("id") Long id) {
        return bookService.updateBook(book, id);
    }

    // DELETE
    @DeleteMapping("/books/{id}")
    public String deleteBook(@PathVariable("id") Long id) {
        bookService.deleteBook(id);
        return "Supprimé avec succès !";
    }

    // GET by title
    @GetMapping("/books/title/{title}")
    public List<Book> findByTitle(@PathVariable("title") String title) {
        return bookService.findByTitle(title);
    }
}


