package co.simplon.SpringBootApiRest.controller;

import org.springframework.web.bind.annotation.RestController;

import co.simplon.SpringBootApiRest.model.Author;
import co.simplon.SpringBootApiRest.repository.AuthorRepository;

import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

@RestController
@RequestMapping("/authors")
public class AuthorController {

    private AuthorRepository authorRepository;

    public AuthorController(AuthorRepository authorRepositoryInjected) {
        this.authorRepository = authorRepositoryInjected;
    }

    @GetMapping("")
    public List<Author> getAll() {
        return this.authorRepository.findAll();
    }

    @PostMapping("")
    public Author postOne(@RequestBody Author author) {
        return this.authorRepository.save(author);
    }
    
}
